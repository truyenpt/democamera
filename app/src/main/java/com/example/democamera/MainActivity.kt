package com.example.democamera

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import com.otaliastudios.cameraview.*
import com.otaliastudios.cameraview.controls.Audio
import com.otaliastudios.cameraview.controls.AudioCodec
import com.otaliastudios.cameraview.controls.Flash
import com.otaliastudios.cameraview.controls.Mode
import java.io.File

class MainActivity : AppCompatActivity() {
    private var video1: String? = null
    private var recording: Boolean = false
    private var camera: CameraView? = null
    private var tvRecord: TextView? = null
    private var tvStop: TextView? = null
    private var tvStatus: TextView? = null
    private var tvPath: TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        camera = findViewById(R.id.camera)
        tvRecord = findViewById(R.id.tvRecord)
        tvStop = findViewById(R.id.tvStop)
        tvStatus = findViewById(R.id.tvStatus)
        tvPath = findViewById(R.id.tvPath)
        tvRecord!!.setOnClickListener {
            tvStatus!!.text = "Đang Quay ...."
            tvPath!!.text = "Đang Chờ Stop"
            startRecordVideo()
        }
        tvStop!!.setOnClickListener {
            tvStatus!!.text = "Sẵn sàng quay mới"
            stopRecordVideo()
        }
        camera!!.setLifecycleOwner(this)

        camera!!.clearCameraListeners()
        camera!!.addCameraListener(object : CameraListener() {
            override fun onCameraClosed() {
                super.onCameraClosed()
                Log.d("TAG", "onCameraClosed: ")
            }

            override fun onCameraOpened(options: CameraOptions) {
                super.onCameraOpened(options)
            }

            override fun onVideoTaken(result: VideoResult) {
                super.onVideoTaken(result)
                onResultVideo(result.file.path)
            }

            override fun onCameraError(exception: CameraException) {
                super.onCameraError(exception)
                Toast.makeText(this@MainActivity, "System error!", Toast.LENGTH_SHORT).show()
                finish()
            }

            override fun onPictureTaken(result: PictureResult) {
                Log.d("TAG", "onPictureTaken: ")
            }
        })
    }

    private fun onResultVideo(path: String) {

        tvPath!!.text = path
        Toast.makeText(this, path, Toast.LENGTH_SHORT).show()

    }

    private fun startRecordVideo() {

        val file =
            File("/storage/emulated/0/Pictures/DemoCamera" + "/Unity_${System.currentTimeMillis()}.mp4")

        if (file.parentFile?.exists() == false) {
            file.parentFile?.mkdir()
        }
        if (file.exists()) {
            file.delete()
        }
        video1 = file.absolutePath
        camera!!.useDeviceOrientation = false
        camera!!.mode = Mode.VIDEO
        recording = true

        camera!!.audio = Audio.OFF
        camera!!.takeVideoSnapshot(file)
    }

    private fun stopRecordVideo() {
        camera!!.stopVideo()
        recording = false

    }

    override fun onDestroy() {
        super.onDestroy()

        try {
            camera!!.clearCameraListeners()
            camera!!.clearFrameProcessors()
            camera!!.close()
            camera!!.destroy()

        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

}